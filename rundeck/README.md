RUNDECK: É um projeto opensource para você executar jobs e comandos. Tem um painel onde vc pode executar os jobs, com ele não precisa de um SysAdm para fazer a execução dos comandos qualquer pessoa pode fazer a execução dos comandos.

# Instalação
- Community - Software livre
- Enterprise - Software proprietário
## https://docs.rundeck.com/docs/administration/install/system-requirements.html

- cd /etc/rundeck
- ls -l
- vim rundeck-config.properties
- vim framework.properties

- ls /etc/init.d
- sudo service rundeckd restart
- sudo service rundeckd status
- cd /var/log/rundeck/
- tail -f rundeck.log

- http://34.95.244.77:4440/user/login

## Criar projeto

## List packages
- apt list
- sudo apt list --installed
- apt list | grep -i rundeck
- sudo apt install aptitude
- apt remove rundeckpro-enterprise

- poweroff

- sudo apt purge rundeckpro-enterprise

- sudo apt-get remove --auto-remove rundeckpro-enterprise

- aptitude purge rundeckpro-enterprise

rundeck-cli/any 1.3.4-1 all
rundeckpro-cluster/any 3.0.8.20181029-1 all
rundeckpro-dr/any 3.0.5.20180828-1 all
rundeckpro-enterprise/any,now 3.3.11.20210507-1 all [installed]
rundeckpro-team/any 3.0.27.20191204-1 all

dpkg: warning: while removing rundeckpro-enterprise, directory '/var/lib/rundeck/var' not empty so not removed
dpkg: warning: while removing rundeckpro-enterprise, directory '/var/lib/rundeck/data' not empty so not removed
dpkg: warning: while removing rundeckpro-enterprise, directory '/var/log/rundeck' not empty so not removed
dpkg: warning: while removing rundeckpro-enterprise, directory '/var/lib/rundeck/libext' not empty so not removed
dpkg: warning: while removing rundeckpro-enterprise, directory '/etc/rundeck' not empty so not removed

## Rundeck web administration
- Criar projetos
    - jobs: É um comando ou conjunto de comandos (scripts) que será executado.
        - options
        - workflow
            - touch /tmp/demo-rundeck
        - add step

## Adicionar outros NOS no projeto
- cd var/rundeck/projects/your-project/etc
    - vim resources.xml

## https://www.youtube.com/watch?v=kE3wxQSMaio

## Permition
- chown -R rundeck.rundeck /home/zantonio_5a/scripts

## Organizar por projetos/Rundeck - Dev e Homologação (Staging)
    - Usar comandos para executar
    - Usar script

    - Rundeck
        - /

# Create rundeck Projects
- Dev-ValidBio
- Dev-Sign2Go
- Dev-ViDaas
- Dev-ASODigital
- Dev-SharedServices
- Dev-Monitoramento

- Project to start and stop Kubernetes Cluster and Instances on GCP Cloud provider

# Create rundeck Jobs
## start-cluster-clustername
    - touch /home/zantonio_5a/scripts/dev-validbio-cluster-started
## start-cluster-clustername
    - touch /home/zantonio_5a/scripts/dev-sign2go-cluster-started
    - touch /home/zantonio_5a/scripts/dev-vidaas-cluster-started
    - touch /home/zantonio_5a/scripts/dev-asodigital-cluster-started
    - touch /home/zantonio_5a/scripts/dev-sharedservices-cluster-started
    - touch /home/zantonio_5a/scripts/dev-monitoramento-cluster-started

# Example
## Project
### Dev-Monitoramento
- Project to start and stop Kubernetes Cluster and Instances on GCP Cloud provider
## Jobs
### start-cluster
- Job to start democlustercli Kubernetes Cluster
- touch /home/zantonio_5a/scripts/dev-monitoramento-cluster-started
### stop-cluster
- Job to stop democlustercli Kubernetes Cluster
- touch /home/zantonio_5a/scripts/dev-monitoramento-cluster-stopped

### executar script
- /home/zantonio_5a/scripts/start_cluster
#### Leu o script porém deu erro no gcloud command, talvez será necessário baixar um plugin
- https://resources.rundeck.com/plugins/google-cloud-platform-nodes/

#### Ver permissão do usuário rundeck no host 
- ESTUDAR PERMISSÕES
- chown -R rundeck.rundeck /home/zantonio_5a/scripts

- chmod -R 777 directory/
- Don't give 777 to any directory!
- https://unix.stackexchange.com/questions/101073/how-to-change-permissions-from-root-user-to-all-users
- chown user:group filename

## ps aux | grep "google"
## sudo chown -R zantonio_5a /usr/bin/
## sudo chown -R zantonio_5a /usr/share/
## 

### gcp-root-password-vm-instance-created-google-cloud-platform
- https://openwritings.net/pg/gcp/gcp-root-password-vm-instance-created-google-cloud-platform#:~:text=By%20default%2C%20Google%20Cloud%20Platform,command%20and%20follow%20the%20instructions.

## chown root:root -f /usr/bin
## visudo

## Reset root password
- https://stackoverflow.com/questions/55547783/how-to-reset-root-password-of-a-google-compute-engine-vm#:~:text=1%20Answer&text=To%20reset%20a%20root%20password,as%20suggested%20in%20that%20post.

- https://stackoverflow.com/questions/35016795/get-root-password-for-google-cloud-engine-vm#answer-44257573

## 
ls -lha
system enable logstash
system enable rundeckd


## Correção do erro 