/* MySQL Server */
https://www.w3schools.com/sql/sql_syntax.asp

/* Para usar o My SQL via terminal - Docker container */
mysql -u root -p /* mysql -u <USER> -p <PWD> */
mysql -u root -h 34.95.255.12 -p

select host from mysql.user where user = 'root'; /* Mostra os hosts que podem acessar o servidor */
create user 'root'@'187.122.5.27' identified by 'mypassword';
grant all privileges on *.* to 'root'@'187.122.5.27';

/* Sistema de login PHP + MySQL */

show databases;

/* */
create database usuarios; /* drop database usuarios; */

/* */
show databases;

/* */
use usuarios;

/* */
create table users(
	codigo int not null primary key auto_increment,
    nome varchar(50) not null unique,
    sobrenome varchar(50),
    sexo tinyint null,
    email varchar(50),
    senha varchar(30),
    niveldeacesso tinyint,
    datadecadastro datetime default current_timestamp
);

/* */
describe users; /* Mostra a estrutura da tabela */
desc users; /* Mostra a estrutura da tabela */
SHOW COLUMNS FROM users; /* Mostra a estrutura da tabela */
SHOW create table users; /* Mostra o código que cria a tabela */
EXPLAIN users; /* Mostra a estrutura da tabela */

/* */
drop table users;

/* */
insert into users values(
	'id'
	'nome', 
    'sobrenome', 
    'sexo', 
    'email', 
    'senha', 
    'niveldeacesso', 
    'datadecadastro'
);

/* Inserir o primeiro usuário */
insert into users values(
	1,
	'Zaqueu', 
    'Ricardo', 
	1, 
    'zaqueuricheng@teste.com', 
    '123456', 
    1,
    now() /* Pega a data do systema*/
);

/* */
SELECT * FROM users;
SELECT column_name_1, column_name_2 FROM TABLE_NAME;

/* Inserir o segundo usuário */
insert into users values(
	2,
	'Emaculada', 
    'Fernandes', 
	1, 
    'fernandesema@teste.com', 
    '123456', 
    1,
    now()
);

/* Inserir o terceiro usuário */
insert into users values(
	3,
	'Leticia', 
    'Ricardo', 
	1, 
    'leisrichema@teste.com', 
    '123456', 
    1,
    now()
);

/* */
select *from users;