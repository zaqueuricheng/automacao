## ELASTIC SEARCH

- kubectl create ns elasticsearch

- elastic helm chart
- https://helm.sh/
- https://artifacthub.io/packages/helm/bitnami/elasticsearch
- https://github.com/elastic/helm-charts/blob/master/elasticsearch/values.yaml

# Install elastic search chart
- helm repo list
- helm repo add bitnami https://charts.bitnami.com/bitnami
- helm repo update
## elastic
- helm search repo elasticsearch
- helm inspect values bitnami/elasticsearch > elastic-values.yaml
- helm install elastic-review --namespace=elasticsearch -f elastic-values.yaml bitnami/elasticsearch --version 15.10.0
- helm upgrade elastic-review --namespace=elasticsearch -f elastic-values.yaml bitnami/elasticsearch --version 15.10.0
- helm uninstall elastic-review --namespace=elasticsearch
## Basics
- 
## Port-foward
- kubectl port-forward elastic-review-elasticsearch-coordinating-only-fcb645489-pfqh4 9200:9200 -n elasticsearch
- kubectl port-forward --namespace elasticsearch svc/elastic-review-elasticsearch-coordinating-only 9200:9200 & curl http://127.0.0.1:9200/

- kubectl describe pod elastic-review-elasticsearch-coordinating-only-7fbfc4c978-szdvk -n elasticsearch

- http://localhost:9200/ // Para verificar se o elastic está funcionando
