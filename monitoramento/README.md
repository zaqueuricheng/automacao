- Elastic search
- Kibana - 
- Kubernetes
- Zabbix 
- Grafana
- Outros...

- https://www.youtube.com/watch?v=Bb3g8xk0Cys
# Elastic search (Ferramenta de big data)
    - Banco de dados (NoSQL) orientado a documentos. 
    - Engine de busca
    - Analise de dados
    - Escalável
    - API Rest

# Logstash
    - Manipulador de logs
    - Engine de coleta de dados em tempo real
    - Trabalha com pipeline
    - Recebe dados de multiplas fontes
    - Envia dados para multiplas fontes
    - Tem diversos plugins para facilicar 

# Kibana
    - Roda no elastic search
    - Integrado com elastic search
    - Visualização e exploração de dados
    - Monitoramento de aplicações
    - Inteligencias operacionais
    - Diversas integrações
    - Dashboards
    - Graficos interativos
    - Suporte a mapa

# Beats
    - Agente coletor de dados
    - Rodam nas máquinas e servidores para coletar dados e enviar ao Logstash ou no elastic search
    - Integrado facilmente com o elastic search ou logstash
    - Logs, métricas, network data, audit data, uptime monitoring
    - Você pode construir seu propio beats (plugins)
    - Instalação dos beats no Kubernetes

## ZABBIX (https://4linux.com.br/o-que-e-zabbix/)
- Server - Instalado no servidor juntamente com o banco de dados (MySQL, PostgreSQL ou Oracle)
- Proxy - Ajuda a diminuir a carga do server
- Agent - Instalado nos micros (hosts)

- https://artifacthub.io/packages/olm/community-operators/elastic-cloud-eck