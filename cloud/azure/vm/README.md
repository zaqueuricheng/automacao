# Criate vm, clusters using azure portal
    - Resource group names: my-vms | my-virtualmachines | my-servers | myclusters | neuron
    - Virtual machine names: demovm | htpdserver | nginxserver | gitlabserver
    - Clusters name: develop | homolog | prod

# Criate vm using command line
    - apt install azure-cli
    - az -h / az --help
    - az login

    - az group create
    - az vm create/resize/delete
    - az vm start/restart/stop

    - az group create --location northeurope --resource-group my-virtualmachines --verbose

    - az vm image list

    - az vm create --name htpdserver --resource-group my-servers --image UbuntuLTS --generate-ssh-keys --output json --verbose
    - az vm create --name htpdserver --resource-group my-virtualmachines --image UbuntuLTS --generate-ssh-keys --output json --verbose

# Connect to virtual machine using local machine

# Create a AKS clusters
- workspace
- az login

- az group create --resource-group neuron --location northeurope --verbose
- az group create --name neuron --location eastus

- az aks create // Mostra todos os parametros que podem ser utilizados com o comando "aks create"
- NAME="develop" RESOURCE_GROUP_NAME="neuron" NODE_VM_SIZE=1 VM_SET_TYPE="standard_a2" LOCATION="westus" 
- az aks create --name NAME --resource-group RESOURCE_GROUP_NAME --node-vm-size NODE_VM_SIZE --vm-set-type VM_SET_TYPE --location LOCATION --verbose

- az aks create --resource-group neuron --name develop --generate-ssh-keys --vm-set-type VirtualMachineScaleSets --load-balancer-sku standard --node-count 1 --zones 1 2 3 --verbose

- az aks create --resource-group myclusters --name homolog --vm-set-type VirtualMachineScaleSets --load-balancer-sku standard --node-count 1 --zones 1 2 3 --verbose

- az aks create -n foo -g neuron --location ukwest
- az aks create -n foo -g neuron --location eastus

- az aks create -g neuron -n MyManagedCluster --kubernetes-version 1.16.9

# https://docs.microsoft.com/en-us/azure/aks/kubernetes-walkthrough
- az group create --name neuron --location eastus
- az aks create --resource-group neuron --name develop --node-count 1 --enable-addons monitoring --generate-ssh-keys
- az aks create --resource-group neuron --name homolog --node-count 1 --enable-addons monitoring --generate-ssh-keys

- az account list-locations
- az group create --name neuron --location brazilsouth
- az aks create --name develop --resource-group neuron --node-count 1
- az aks create --name homolog --resource-group neuron --node-count 1
- az aks create --name prod --resource-group neuron --node-count 1

- az group create --name neuron-dev --location brazilsouth
- az aks create --name develop --resource-group neuron-dev --node-count 1
- az group create --name neuron-hml --location brazilsouth
- az aks create --name hml --resource-group neuron-hml --node-count 1
- az group create --name neuron-prd --location brazilsouth
- az aks create --name prd --resource-group neuron-prd --node-count 1

# Terraform - https://learn.hashicorp.com/tutorials/terraform/install-cli?in=terraform/azure-get-started
# Provisionar

# 1 - install
# 2 - terraform -help
# 3 - terraform -help plan