#!/bin/bash

# forcing profile in my script to run gcloud command
source /etc/profile

# logs to monitore all time that the service is started
# /home/zantonio_5a/scripts

echo "start cluster works" >> /home/node-01/start_works.txt
date >> /home/node-01/start_works.txt
echo "" >> /home/node-01/start_works.txt
echo "#####################################################" >> /home/node-01/start_works.txt
echo "" >> /home/node-01/start_works.txt

# variables
CLUSTER_NAME="democlustercli"
CLUSTER_ZONE="us-central1-a"
CLUSTER_REGION=""

# test variables if it is fine
echo $CLUSTER_NAME >> /home/node-01/stop_works.txt
echo $CLUSTER_ZONE >> /home/node-01/stop_works.txt

# command to resize cluster for start services of nodes
yes Y | gcloud container clusters resize $CLUSTER_NAME --size=1 --zone $CLUSTER_ZONE

yes Y | gcloud container clusters resize "democlustercli" --num-nodes=1 --zone "us-central1-a"
