# start.sh
# stop.sh
# Conectar via gcloud command - precisa ter o putty instalado
- gcloud compute instances list
- gcloud compute ssh demoinstancecli --zone="europe-central2-c"

- gcloud beta compute ssh --zone "southamerica-east1-b" "monitoramento-gcp"  --project "monitoramento-valid"
# Executar comandos dentro da máquina
    - sudo su
    - apt-get update
    - apt-get upgrade

    - apt install neofetch
    - apt install gcp
    - apt install git
    - snap install kubectl --classic
    - apt install cron
    - date
    - neofetch
    - htop
# Install packages
    - sudo snap install google-cloud-sdk --classic
    - gcloud auth login
    - gcloud projects list
    - gcloud config set project
    - gcloud compute instances list
    - gcloud container clusters list
    - gcloud components list
    - gcloud components update
# Ubuntu
    - mkdir scripts
    - touch start_cluster
        - vim start_cluster
            - ESC
            - S // to insert data
            - set nu // to numeber the line of the editor
            - wq
            - q!
            - /home/zantonio_5a/scripts
        - ls -l
        - chmod +x start_cluster
        - gcloud auth login
        - gcloud projects list
        - gcloud config set project
    - crontab
        - crontab -e
        - crontab -l
        - sudo vim /etc/crontab
        - minuto hora dia  mes   dia-da-semana               comando
        - 0-59   0-23 1-31 1-12  0-7 (0=domingo, 7=domingo)  comando-a-ser-executado
        - example
            - 0 10 * * 1-5 // 0min 10h todos-os-dias todos-meses de-segunda-a-sexta
            - 0 10 * * 1-5 /home/zantonio_5a/scripts/start-cluster
            - 0 23 * * 1-5 /home/zantonio_5a/scripts/stop-cluster
        - rm -r start_works.txt
        - cp start_cluster /home/zantonio_5a/scripts/stop-cluster
        - export CLUSTER_NAME="democlustercli"
        - export CLUSTER_ZONE="us-central1-a"
        - gcloud container clusters resize $CLUSTER_NAME --num-nodes=0 --zone $CLUSTER_ZONE
        - timedatectl set-timezone America/Sao_Paulo
        - date

*/1 * * * * /home/node-01/start_cluster.sh

    - export CLUSTER_NAME=""
    - chmod +x start_cluster
    - /home/node-01/start_cluster
    - cp start_cluster /home/node-01/stop_cluster

# Vim editor