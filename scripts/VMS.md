az
## Create group and vm
https://adamtheautomator.com/azure-cli/

az --help
az group create
az vm create/resize/delete
az vm start/restart/stop

az group create --location northeurope --resource-group my-virtualmachines --verbose

az vm image list

az vm create --resource-group my-group \
   --name demo \ ## hostname for the virtual machine
   --image UbuntuLTS \ ## Image name gathered with az vm image list
   --generate-ssh-keys \ ## Auto generates SSH keys for use with VM
   --output json \ ## override for format output to return JSON
   --verbose ## extra output

az vm create --resource-group my-virtualmachines --name ubuntuvm --image UbuntuLTS --generate-ssh-keys --output json --verbose
## Create Kubernetes cluster

az group create

az aks --help/create/delete/get-credentials/install-cli
az aks /show/start/restart/stop/update/upgrade

az group create --location northeurope --resource-group my-clusters --verbose
az aks create --resource-group my-clusters --name developCluster

