- https://stackoverflow.com/questions/33755768/how-can-i-cleanly-remove-a-container-image-from-the-google-container-registry

# Auth
    - sudo snap install google-cloud-sdk --classic
    - gcloud auth login
    - gcloud projects list
    - gcloud config set project sign2go-dev
    - gcloud compute instances list
    - gcloud container clusters list
    - gcloud components list
    - gcloud components update

# List images
    - gcloud container images list
    - gcloud container images list-tags gcr.io/sign2go-dev/sign2go-business

    - gcloud container images list-tags gcr.io/sign2go-dev/sign2go-business --filter='-tags:*' --format="get(digest)" --limit=$BIG_NUMBER
    - gcloud container images list-tags gcr.io/sign2go-dev/sign2go-business --filter='-tags:*' --format="get(digest)" --limit=10 > tags && while read p

# Delete images
- gcloud container images delete gcr.io/sign2go-dev/sign2go-business@sha256:cbfe4fef25636264bbb71a75b7340e81b747c22fee4bfe9de349c623d117972d --force-delete-tags

- gcloud container images delete gcr.io/sign2go-dev/sign2go-business@sha256:[digest] --force-delete-tags
- gcloud container images delete gcr.io/sign2go-dev/sign2go-business@sha256:4827b86c3a78 --force-delete-tags

- gcloud container images describe gcr.io/sign2go-dev/sign2go-business:[tag]
- gcloud container images describe gcr.io/sign2go-dev/sign2go-business:2.0.0-86


## Script to delete varios images

$vetor = gcloud container images list-tags gcr.io/sign2go-dev/sign2go-business
echo $vetor
$digest0 = $vetor[0] ## Pega o digest
echo $digest0
$digest1 = $vetor[1] ## Pega a tag
echo $digest1
$digest2 = $vetor[2] ## Espaço em branco
echo $digest2
$digest3 = $vetor[3] ## Pega o digest
echo $digest3
$digest4 = $vetor[4] ## Pega a tag
echo $digest4
$digest5 = $vetor[5] ## Espaço em branco
echo $digest5
$digest6 = $vetor[6] ## Pega o digest
echo $digest6

$digest15 = $vetor[15] ## Pega o digest
echo $digest15


$vetor = gcloud container images list-tags gcr.io/sign2go-dev/sign2go-business

for (i=6; i<100; i++){
    $digest = $vetor[i] ## Pega o digest
    echo $digest
}

gcloud container images delete gcr.io/sign2go-dev/sign2go-business@sha256:$digest --force-delete-tags

- Set-Executionpolicy -Executionpolicy Unrestricted -Scope LocalMachine

.\a_variables.ps1

.\d_stop_cluster.ps1
.\e_start_cluster.ps1

- powershell.exe -noexit -file .\a_variables.ps1
- powershell.exe -noexit -file .\d_stop_cluster.ps1