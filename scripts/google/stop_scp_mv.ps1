# c_stop_instances.ps1

# .\c_stop_instances.ps1
# .\stop_mv.ps1

$PROJECT_ID = "ageless-talent-322500"
$INSTANCE_NAME = ""
$INSTANCE_ZONE = "southamerica-east1-a"

# gcloud compute instances stop --help
# gcloud compute instances stop instance-1 instance-2 instance-3 instance-4

# gcloud beta compute instances stop $INSTANCE_NAME --project=$PROJECT_ID --zone=$INSTANCE_ZONE

gcloud beta compute instances stop "vld-prd-ie-process-001" --zone=$INSTANCE_ZONE
gcloud beta compute instances stop "vld-prd-ie-process-002" --zone=$INSTANCE_ZONE
gcloud beta compute instances stop "vld-prd-ie-process-003" --zone=$INSTANCE_ZONE
gcloud beta compute instances stop "vld-prd-ie-process-004" --zone=$INSTANCE_ZONE
gcloud beta compute instances stop "vld-prd-ie-process-005" --zone=$INSTANCE_ZONE
gcloud beta compute instances stop "vld-prd-ie-pub-001" --zone=$INSTANCE_ZONE
gcloud beta compute instances stop "vld-prd-ie-scpweb-001" --zone=$INSTANCE_ZONE
gcloud beta compute instances stop "vld-prd-ie-scpweb-client-001" --zone=$INSTANCE_ZONE

gcloud beta compute instances stop "vld-prd-ie-send-001" --zone=$INSTANCE_ZONE
gcloud beta compute instances stop "vld-prd-ie-send-002" --zone=$INSTANCE_ZONE
gcloud beta compute instances stop "vld-prd-ie-send-003" --zone=$INSTANCE_ZONE