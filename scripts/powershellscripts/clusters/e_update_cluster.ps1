# e_update_cluster.ps1

# .\e_update_cluster.ps1

$CLUSTER_NAME = "democlustercli"
$CLUSTER_MACHINE_TYPE = "n1-highcpu-2"
$CLUSTER_ZONE = "us-central1-a"
$CLUSTER_REGION = ""

gcloud container clusters update $CLUSTER_NAME --zone `
    $CLUSTER_ZONE --enable-autoscaling --min-nodes 0 --max-nodes 10