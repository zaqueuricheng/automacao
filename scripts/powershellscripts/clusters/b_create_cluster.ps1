# b_create_cluster.ps1

# .\b_create_cluster.ps1

$CLUSTER_NAME = "democlustercli"
$CLUSTER_MACHINE_TYPE = "n1-highcpu-2"
$CLUSTER_ZONE = "us-central1-a"
$CLUSTER_REGION = ""

gcloud container clusters create $CLUSTER_NAME `
    --num-nodes 1 --zone=$CLUSTER_ZONE --machine-type=$CLUSTER_MACHINE_TYPE