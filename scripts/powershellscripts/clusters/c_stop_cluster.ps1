# c_stop_cluster.ps1

# .\c_stop_cluster.ps1

$CLUSTER_NAME = "democlustercli"
$CLUSTER_MACHINE_TYPE = "n1-highcpu-2"
$CLUSTER_ZONE = "us-central1-a"
$CLUSTER_REGION = ""

gcloud container clusters resize $CLUSTER_NAME --num-nodes=0 --zone $CLUSTER_ZONE