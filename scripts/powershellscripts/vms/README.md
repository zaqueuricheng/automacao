# Maquina virtual no GCP
- Para criar máquina virtual (VM) no GCP, primeiro cria-se o "PROJETO" que é parecido com "RESOURCE GROUP" do Microsoft Azure e posteriormente criar as INSTÂNCIAS (VM´S).

## Conexão e geração de ssh-key para se conectar a Instancia (VM´S)

- Instalar o putty
    - https://cloud.google.com/compute/docs/instances/connecting-advanced#windows-putty
    - Use o PuTTYgen
        - Gera a chave (chave, user, password)
        - Salva a chave em modo privado e publico opcionalmente
        - Copia a chave e cola no console em: compute engine->metadata
        - Copia o IP Externo da VM e cola no Putty
        - No Pytty vai em ssh->Auth->Seleciona o arquivo de autenticação
        - Basta conectar e adicionar seu usuário e senha
        - C:Users/User/.ssh/known_hosts ## Para limpar os hosts de autenticação


