# c_stop_instances.ps1

# .\c_stop_instances.ps1

$PROJECT_ID = "velerodemo"
$INSTANCE_NAME = "demoinstancecli"
$INSTANCE_ZONE = "europe-central2-c"

# gcloud compute instances stop --help
# gcloud compute instances stop instance-1 instance-2 instance-3 instance-4

# gcloud beta compute instances stop $INSTANCE_NAME --project=$PROJECT_ID --zone=$INSTANCE_ZONE

gcloud beta compute instances stop $INSTANCE_NAME --zone=$INSTANCE_ZONE