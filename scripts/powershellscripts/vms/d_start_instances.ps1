# d_start_instances.ps1

# .\d_start_instances.ps1

$PROJECT_ID = "velerodemo"
$INSTANCE_NAME = "demoinstancecli"
$INSTANCE_ZONE = "europe-central2-c"

# gcloud compute instances start --help
# gcloud compute instances start instance-1 instance-2 instance-3 instance-4

# gcloud beta compute instances start $INSTANCE_NAME --project=$PROJECT_ID --zone=$INSTANCE_ZONE

gcloud beta compute instances start $INSTANCE_NAME --zone=$INSTANCE_ZONE