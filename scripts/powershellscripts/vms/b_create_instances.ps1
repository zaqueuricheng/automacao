# b_create_instances.ps1

# # .\b_create_instances.ps1

$PROJECT_ID = ""
$INSTANCE_REGION = ""

$INSTANCE_NAME = "demoinstancecli"
$INSTANCE_ZONE = "europe-central2-c" # southamerica-east1-a | southamerica-east1-b
$INSTANCE_MACHINE_TYPE = "e2-small" # e2-standard-2 | e2-standard-8 | e2-standard-4

# gcloud compute images list
$INSTANCE_IMAGE_FAMILY = "ubuntu-2104"
$INSTANCE_IMAGE_PROJECT = "ubuntu-os-cloud"
$INSTANCE_DISK_SIZE = "10GB"

# gcloud compute instances list
# gcloud compute machine-types list
# gcloud compute images list
# gcloud compute disk-types

gcloud compute instances create $INSTANCE_NAME `
    --zone=$INSTANCE_ZONE --machine-type=$INSTANCE_MACHINE_TYPE `
    --image-family=$INSTANCE_IMAGE_FAMILY --image-project=$INSTANCE_IMAGE_PROJECT `
    --boot-disk-size=$INSTANCE_DISK_SIZE