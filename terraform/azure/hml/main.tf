# Configure the Azure provider
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.65"
    }
  }

  required_version = ">= 0.14.9"
}

provider "azurerm" {
  features {}
}

# Create a resource group
resource "azurerm_resource_group" "rg" {
  name     = var.resource_group_name
  location = "brazilsouth"

  tags = {
     Environment = "Terraform Getting Started"
     Team = "DevOps"
  }
}

# Create a virtual network
resource "azurerm_virtual_network" "vnet" {
    name                = "neuronvnet"
    address_space       = ["10.0.0.0/16"]
    location            = "brazilsouth"
    resource_group_name = azurerm_resource_group.rg.name
}

# Create a kubernetes cluster
resource "azurerm_kubernetes_cluster" "cluster_hml" {
  name                = "neuron_hml"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  dns_prefix          = "neuronhml"

  default_node_pool {
    name       = "default"
    node_count = "2"
    vm_size    = "standard_d2_v2"
  }

  identity {
    type = "SystemAssigned"
  }
}