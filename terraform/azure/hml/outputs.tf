resource "local_file" "kubeconfig" {
  depends_on   = [azurerm_kubernetes_cluster.cluster_hml]
  filename     = "kubeconfig"
  content      = azurerm_kubernetes_cluster.cluster_hml.kube_config_raw
}