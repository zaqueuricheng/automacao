resource "local_file" "kubeconfig" {
  depends_on   = [azurerm_kubernetes_cluster.cluster_prod]
  filename     = "kubeconfig"
  content      = azurerm_kubernetes_cluster.cluster_prod.kube_config_raw
}