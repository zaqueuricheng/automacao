resource "local_file" "kubeconfig" {
  depends_on   = [azurerm_kubernetes_cluster.cluster_dev]
  filename     = "kubeconfig"
  content      = azurerm_kubernetes_cluster.cluster_dev.kube_config_raw
}