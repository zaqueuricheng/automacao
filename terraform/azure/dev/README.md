- terraform init
- terraform fmt
- terraform validate
- terraform apply
- terraform show
- terraform state list
- terraform state
- terraform destroy

# https://learnk8s.io/terraform-aks