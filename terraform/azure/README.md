- https://www.terraform.io/
- https://learn.hashicorp.com/tutorials/terraform/azure-variables?in=terraform/azure-get-started
- https://learnk8s.io/terraform-aks

- https://jenkins.clarobrasil.mobi/job/MVNO/
- https://gitdev.net.com.br/users/sign_in
- https://corpclarobr.sharepoint.com/_forms/default.aspx

- https://gitlab.com/zaqueuricheng

- https://portal.azure.com/#@zaqueu92804hotmail.onmicrosoft.com/resource/subscriptions/fe0f8c3a-1822-4eee-8e70-826d8af32379/quotas

- https://portal.azure.com/#@zaqueu92804hotmail.onmicrosoft.com/resource/subscriptions/fe0f8c3a-1822-4eee-8e70-826d8af32379/resourceGroups/neuron/overview

## Install terraform
- https://www.terraform.io/downloads.html
- C:\Users\zantonio.5a\.terraform

## Terraform commands
- terraform init
- terraform fmt
- terraform validate
- terraform apply
- terraform show
- terraform state list
- terraform state
- terraform destroy

# https://learnk8s.io/terraform-aks