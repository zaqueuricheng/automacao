terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "3.5.0"
    }
  }
}

## credentials
provider "google" {
  credentials = file("credentials.json") ## Não esta sendo utilizado, está pegando as credentials do computador
  project = "monitoramento-valid"
  region  = "us-central1"
  zone    = "us-central1-c"
}

## create virtual private cloud
resource "google_compute_network" "vpc_network" {
  name = "terraform-network"
}

## criate instance 1 - Ubuntu
resource "google_compute_instance" "vm_instance_1" {
  name         = "terraform-instance-1"
  machine_type = "f1-micro"

  ## image OS
  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-minimal-2104" ## gcloud compute images list | Select-String -Pattern 'ubuntu' (project/family)
    }
  }

  ## network for instance
  network_interface {
    network = google_compute_network.vpc_network.name
    access_config {
    }
  }
}

## create instance 2 - Debian
resource "google_compute_instance" "vm_instance_2" {
  name         = "terraform-instance-2"
  machine_type = "n2d-highcpu-8" ## gcloud compute machine-types list --filter="zone:( us-central1-b europe-west1-d )"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  network_interface {
    network = google_compute_network.vpc_network.name
    access_config {
    }
  }
}

## create instance 3 - Windows
resource "google_compute_instance" "vm_instance_3" {
  name         = "terraform-instance-3"
  machine_type = "e2-standard-2"

  boot_disk {
    initialize_params {
      image = "windows-cloud/windows-2004-core"
    }
  }

  network_interface {
    network = google_compute_network.vpc_network.name
    access_config {
    }
  }
}

## https://learn.hashicorp.com/tutorials/terraform/google-cloud-platform-change?in=terraform/gcp-get-started
## https://blog.avenuecode.com/how-to-use-terraform-to-create-a-virtual-machine-in-google-cloud-platform
## https://stackoverflow.com/questions/66113323/terraform-error-creating-firewall-googleapi-error-403-required-compute-fi

## https://www.youtube.com/watch?v=JQYgFSYFi-o