- terraform init
- terraform fmt
- terraform validate
- terraform apply
- terraform show
- terraform state list
- terraform state
- terraform destroy

# https://learnk8s.io/terraform-aks


## Create variables to access cluster for pipeline
## 1 - Create service account (e-mail) - sign2go-business-registry, sign2go-business-dev, sign2go-business-hml, sign2go-business-prod
- gcloud iam service-accounts create terraform-access
- gcloud iam service-accounts list | Select-String -Pattern 'sign2go-business-prod'
## 2 - Create credentials
- $SERVICE_ACCOUNT_EMAIL="sign2go-business-prod@sign2go-prd.iam.gserviceaccount.com"
- gcloud iam service-accounts keys create credentials-sign2go-business-prod --iam-account $SERVICE_ACCOUNT_EMAIL
## Create variables on pipeline files graphical environment
- AUTH_JSON_REGISTRY - credentials of gcloud registry/sign2go-dev

- AUTH_JSON_KUBE_DEV - credentials of gcloud dev project/sign2go-dev
- AUTH_JSON_KUBE_HML - credentials of gcloud hml project/sign2go-hml
- AUTH_JSON_KUBE_PRD - credentials of gcloud prod project/sign2go-prod


###### Terraform
- gcloud iam service-accounts list
- gcloud iam service-accounts create terraform-access
- gcloud iam service-accounts list | Select-String -Pattern 'terraform-access'
- $SERVICE_ACCOUNT_EMAIL="terraform-access@monitoramento-valid.iam.gserviceaccount.com"
- gcloud iam service-accounts keys create credentials-terraform --iam-account $SERVICE_ACCOUNT_EMAIL

## ------------------------------------------------------------------ ##
## Create credentials
- https://blog.avenuecode.com/how-to-use-terraform-to-create-a-virtual-machine-in-google-cloud-platform

## vpc works!!!
- https://learn.hashicorp.com/tutorials/terraform/google-cloud-platform-build?in=terraform/gcp-get-started


## Credentials path
credentials = file("C:/works/valid/automacao/terraform/google/dev/monitoramento-valid-44c6329f6424.json")